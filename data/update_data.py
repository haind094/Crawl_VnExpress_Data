"""
========================
Author: Nguyen Dinh Hai |
ver   : 2.2             |
Date  : Sun Jun 7, 2020 |
========================
"""

from .content import get_link_article, get_data, get_next_page


def check_new(data_table, page='https://vnexpress.net/tin-tuc/giao-duc'):
    """Check new article
    :param data_table: the table of data base
    :param page: the first page
    :return: if no new article return None, else return new href
    """
    new_article = list()
    state = 0
    while state == 0:
        url_one_page = get_link_article(page)
        for link_article in url_one_page:
            print(link_article)
            if find_in_database(data_table, link_article) is not None:
                state = 1
                break
            else:
                new_article.append(link_article)
        page = get_next_page(page)
    return new_article


def find_in_database(data_table, href):
    """To find a element href in database
    :param data_table:
    :param href:
    :return:
    """
    return data_table.find_one({'Address': href})


def update(data_table):
    """To update if service have new article
    :param: data_table: the table of database
    :return: notify the statement
    """
    new_links = check_new(data_table)
    if new_links is not None:
        for link in new_links:
            data = get_data(link)
            data_table.insert(data, check_keys=False)
        return print("The data was updated!")
    else:
        return print("The home page have no new article!")

