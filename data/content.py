"""
========================|
Author: Nguyen Dinh Hai |
ver   : 2.2             |
Date  : Sun Jun 7, 2020|
========================|
"""

import time
import requests
import json

from bs4 import BeautifulSoup


# To check and auto connect in times out
def check_internet(URL):
    """Check internet have available
    :param URL: link which we need to check
    :return: False when it is loss, and page when ok
    """
    try:
        page = requests.get(URL)
        if page.status_code is 200:
            return page
    except requests.ConnectionError as notification:
        print(notification)
        return False


def auto_connect(URL):
    """To reconnect internet when it was loss
    :return: page follow request method
    """
    time_out = 1
    while check_internet(URL) is False:
        print('Connecting...')
        time_out += 3
        time.sleep(time_out)
        check_internet(URL)
        if time_out > 15:
            break
    return check_internet(URL)


# To get comment
def extra_comment(article_id):
    """ Detail: Render comment from page of article
    :param article_id: id of article
    :return:
    """

    if (article_id['article_type'] is 'text') or \
            (article_id['article_id'] is None):
        return None
    else:
        return (get_comment(get_ulr_cmt(
            article_id)))


# Extra comment from api of website to dictionary
def get_comment(api_comment):
    """ Task: get comment from api of page article
    :param api_comment is link api get from home page, where is contain comment
    :return:  None when not cmt and comment when it have comment in page
    """
    comment = dict()

    page_cmt = auto_connect(api_comment)
    result = BeautifulSoup(page_cmt.content, "html.parser")
    data = json.loads(result.getText())

    if data['data']['total'] is 0:
        return None
    else:
        for i in range(len(data['data']['items'])):
            comment[data['data']['items'][i]['full_name']] = \
                data['data']['items'][i]['content']
            return comment
        else:
            return None


def get_ulr_cmt(article_id):
    """Extra URL api of comment from article_id
    :param article_id: article id of article
    :return: URL api
    """

    api_link = 'https://usi-saas.vnexpress.net/index/' \
               'get?offset=0&limit=15&frommobile=0&sort=like&is_onload=1&objectid=' \
               + str(article_id['article_id']) \
               + '&objecttype=' + str(article_id['article_type']) \
               + '&siteid=1000000'

    return api_link


def get_article_id(result):
    """To extra article_id of article
    :param result: page of article we need to crawl
    :return: article id
    """
    article_element = dict()

    try:
        tmp = result.find('div', class_='box_comment_vne box_category width_common'). \
            get('data-component-input')
        article_id = json.loads(tmp)['article_id']
        article_type = json.loads(tmp)['article_type']
    except AttributeError:
        try:
            tmp = result.find('section', class_='section page-detail top-detail'). \
                get('data-component-config')
            article_id = json.loads(tmp)['article_id']
            article_type = json.loads(tmp)['type']
        except AttributeError:
            article_id = None
            article_type = None

    article_element['article_type'] = article_type
    article_element['article_id'] = article_id

    return article_element


# Get all of the link article from homepage
def get_link_article(
        page='https://vnexpress.net/tin-tuc/giao-duc'):
    """Get link for every article
    :param page: home page which we need to crawl
    :return: all of link in home page
    """
    page = auto_connect(page)
    soup = BeautifulSoup(page.content, "html.parser")
    all_links = []
    for link in soup.find_all('',
                              {'class': {'title-news', 'title_new'}}):
        all_links.append(link.find('a')['href'])
    return all_links


# Get link homepage
def get_next_page(page='https://vnexpress.net/tin-tuc/giao-duc',
                  url='https://vnexpress.net'):
    """Extra next page
    :param page: previous page
    :param url: home page
    :return: next page
    """

    page = auto_connect(page)
    soup = BeautifulSoup(page.content, "html.parser")
    check = soup.find('a', class_='btn-page next-page')

    if check is None:
        return None
    else:
        url_update = check.get('href')
        print(url + url_update)
        return url + url_update


# Get content and title for each article


def get_title(soup):
    """Get title from article
    :param soup: the output from beautifulSoup
    :return: tile of page
    """
    title_name = soup.title.getText()
    return title_name


def get_content_article(soup):
    """Detail: only the content of the article
    :param soup: the output from beautifulSoup
    :return: content
    """
    # Find all the content tab
    content = str()
    # Join all of the part content from tab to one string
    for list_one in soup.find_all("p", class_="Normal"):
        content = content + (list_one.getText())
    if content == '':
        for list_two in soup.find_all('p'):
            content = content + (list_two.getText())
    return content


def get_data(page):
    """ Get all of content follow request
    + Tile
    + Content
    + Comment
    :param page:
    :return:
    """
    page_request = auto_connect(page)
    soup = BeautifulSoup(page_request.content, "html.parser")
    title_name = get_title(soup)
    content = get_content_article(soup)
    article_id = get_article_id(soup)
    comment = extra_comment(article_id)
    diction = {'Title': title_name, 'Content': content,
               'Comment': comment, 'Address': page}
    return diction
