"""
========================|
Author: Nguyen Dinh Hai |
ver   : 2.2             |
Date  : SUN Jun 08, 2020|
========================|
"""

import time
import pymongo

import threading
from threading import Thread

from data.update_data import update
from data.content import get_link_article, \
                         get_data, \
                         get_next_page


def init_database():
    """To innit data base
    :param :
    :return: name of table database
    """
    # Connect to MongoDB and create new database
    name_database = "VnExpressArticle"
    my_client = pymongo.MongoClient("mongodb://localhost:27017/")

    # Create new client
    my_Database = my_client["VnExpress"]

    # Create new database
    data_table = my_Database[name_database]
    return data_table


def check_mode(data_table):
    """To check mode for program will be run
    :param data_table: database table
    :return: the mode function need to run
    """

    if data_table.find():
        return 'update'
    else:
        return None


def write_data(link_article, data_table):
    data = get_data(link_article)
    data_table.insert(data, check_keys=False)
    return None


class Thread_With_Return_Value(Thread):
    """Return value of thread
    """

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        print(type(self._target))
        if self._target is not None:
            self._return = self._target(*self._args,
                                        **self._kwargs)

    def join(self, *args):
        Thread.join(self, *args)
        return self._return


def crawl(data_table, url_1='https://vnexpress.net/tin-tuc/giao-duc'):
    while url_1 is not None:
        all_link_article = get_link_article(url_1)

        # Run thread get link page
        thread_get_next_page = Thread_With_Return_Value(
            target=get_next_page, args=(url_1,))
        thread_get_next_page.start()
        thread = [str(i) for i in range(len(all_link_article))]

        # Run thread
        for i in range(len(all_link_article)):
            if all_link_article[i].find('https://vnexpress.net/') is -1:
                continue
            thread[i] = threading.Thread(target=write_data,
                                         args=(all_link_article[i], data_table,))
            thread[i].start()

        # Wait thread end
        for i in range(len(all_link_article)):
            if all_link_article[i].find('https://vnexpress.net/') is -1:
                continue
            thread[i].join()

        url_1 = thread_get_next_page.join()
    return print("Finish process!")


if __name__ == '__main__':

    time_start = time.time()

    table = init_database()

    if check_mode(table):
        update(table)
    else:
        crawl(table)

    print("Program execution time:", time.time() - time_start)
